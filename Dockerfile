# Base CERN worker image
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7


# Install needed packages
RUN yum update -y \
    && yum install -y \
        java-1.8.0-openjdk-devel \
        htop \
        mlocate \
        wget \
    && yum clean all \
    && updatedb

# Sets the locales in UTF8
ENV LC_ALL=en_US.UTF-8

# Environment variables
ENV VERSION_SDK_TOOLS="4333796" \
    SDKMANAGER="28" \
    BUILD_TOOL="28.0.3" \
    ANDROID_HOME="/usr/local/"

# Install Android SDK & SDKs
RUN echo "Installing sdk tools" && \
    wget --quiet --output-document=sdk-tools.zip "https://dl.google.com/android/repository/sdk-tools-linux-${VERSION_SDK_TOOLS}.zip" && \
    unzip -q sdk-tools.zip -d "$ANDROID_HOME" && rm --force sdk-tools.zip  && \
    mkdir  --parents "$ANDROID_HOME/.android/" && \
    yes | "$ANDROID_HOME"/tools/bin/sdkmanager --licenses > /dev/null && \
    echo "Installing platforms" && \
    yes | "$ANDROID_HOME"/tools/bin/sdkmanager > /dev/null "platforms;android-$SDKMANAGER" && \
    echo "Installing platform tools " && \
    yes | "$ANDROID_HOME"/tools/bin/sdkmanager > /dev/null "platform-tools" && \
    echo "Installing build tools " && \
    yes | "$ANDROID_HOME"/tools/bin/sdkmanager > /dev/null "build-tools;$BUILD_TOOL"

WORKDIR /app
# For testing uncomment
#COPY ./MyApplication/ .
