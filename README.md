[![pipeline status](https://gitlab.cern.ch/apc/susofts/shared/sus_ci_androidworker/badges/master/pipeline.svg)](https://gitlab.cern.ch/apc/susofts/shared/sus_ci_androidworker/commits/master)


SUS CI AndroidWorker
====================

This project is based on <https://gitlab.cern.ch/DataProcessingAndAnalysis/sus_ci_worker/>. You can find more info about how to use it in the [Registry page](https://gitlab.cern.ch/apc/susofts/shared/sus_ci_androidworker/container_registry) of this repository.

#### Table of content ####

[Goal](#goal)

[Usage](#usage)

[Automatic build](#automatic-build)



Goal
----

We create a docker image that will be used as a job runner (or worker) for Gitlab CI. Its goal will be to compile, run the tests and automatically build a signed APK for the SMART app. 
The image contains all the prerequisites to build and run the Android SDK (so we don't have to reinstall them at each run, which saves a lot of time):

- SDK tools
- Platform tools
- Build Tools

Usage
-----

You can use the container on you local machine. First, you need to install docker. Then:

- build the container with `docker build -t sus_ci_androidworker .`
- run the container: `docker run --rm -it sus_ci_androidworker bash`
	- if you want to start it from *Git for Windows* console, you need to prepend `winpty`:
	- `winpty docker run --rm -it sus_ci_androidworker bash`

We have the following environmental variables:


`ENV VERSION_SDK_TOOLS "4333796"`

You can check for new versions in the Command line tools only [here](https://developer.android.com/studio/#downloads) and update this variable.

`ENV SDKMANAGER "28"`

You can check for new versions [here](https://developer.android.com/studio/command-line/sdkmanager)

`ENV BUILD_TOOL "28.0.3"`

You can check for new versions [here](https://developer.android.com/studio/releases/build-tools)

In order to test uncomment the last line:

COPY ./MyApplication/ .

To build automatically signed APKs with every release we added a keystore file and in build.gradle we add the signingConfigs.
You can see more information in the Android Developers [website](https://developer.android.com/studio/publish/app-signing)

Automatic build
---------------

As soon as you commit to this repository, the image will automatically be built thanks to Gitlab-CI. The automatic build may fail for various reasons:

- CERN infrastructure downtime
- network issue
- unavailable resource (repo Git, Boost...)
- ...

If the error looks strange, don't hesitate to rerun the build, it may work after a few tries...

Note that the image must successfully build to be used as a runner for other project's CI.
